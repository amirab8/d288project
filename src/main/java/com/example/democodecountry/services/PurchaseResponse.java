package com.example.democodecountry.services;

import lombok.*;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin
@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PurchaseResponse {
  private String orderTrackingNumber;
}
