package com.example.democodecountry.entities;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
@CrossOrigin

//@CrossOrigin("http://localhost:4200")
@Entity
@Table(name="divisions")
@Getter
@Setter
public class Division {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name="division_id")
  private Long id;

  @Column(name="division")
  private String division_name;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name="Country_ID", nullable = false, insertable = false, updatable = false)
  private Country country;

  @OneToMany(cascade=CascadeType.ALL, mappedBy = "division")
  private Set<Customer> customers = new HashSet<>();

  @Column (name = "Country_ID")
  private long country_id;
  public void setCountry(Country country)
  {
    setCountry_id(country.getId());
    this.country = country;
  }

  @Column(name="create_date")
  @CreationTimestamp
  private Date create_date;

  @Column(name="last_update")
  @UpdateTimestamp
  private Date last_update;
}

