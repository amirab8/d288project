package com.example.democodecountry.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.Date;
import java.util.Set;
@CrossOrigin

//@CrossOrigin("http://localhost:4200")
@Entity
@Table(name = "countries")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Country {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name="country_id")
  private Long id;

  @Column(name="country")
  private String country_name;

  @OneToMany(cascade=CascadeType.ALL, mappedBy = "country")
  private Set<Division> divisions ;

  @Column(name="create_date")
  @CreationTimestamp
  private Date create_date;

  @Column(name="last_update")
  @UpdateTimestamp
  private Date last_update;
}
