package com.example.democodecountry.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
//import javax.persistence.*;
@CrossOrigin

//@CrossOrigin("http://localhost:4200")
@Entity
@Table(name = "customers")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Customer {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "customer_id")
  private Long id;

  @Column(name = "customer_first_name", nullable = false)
  private String firstName;

  @Column(name = "customer_last_name", nullable = false)
  private String lastName;

  @Column(name = "address", nullable = false)
  private String address;

  @Column(name = "postal_code")
  private String postal_code;

  @Column(name = "phone", nullable = false)
  private String phone;

  @Column(name = "create_date")
  @CreationTimestamp
  private Date create_date;

  @Column(name = "last_update")
  @UpdateTimestamp
  private Date last_update;

  @ManyToOne
  @JoinColumn(name = "division_id")
  private Division division;

  @OneToMany(mappedBy = "customer")
  private Set<Cart> carts = new HashSet<>();

  public Customer(String first, String last, String s, String s1, String s2) {
  }
}
