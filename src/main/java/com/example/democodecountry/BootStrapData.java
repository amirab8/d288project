package com.example.democodecountry;

import com.example.democodecountry.dao.CountryRepository;
import com.example.democodecountry.dao.CustomerRepository;
import com.example.democodecountry.dao.DivisionRepository;
import com.example.democodecountry.entities.Customer;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

  @Component
  public class BootStrapData implements CommandLineRunner
  {
    private final CustomerRepository customerRepository;
    private final DivisionRepository divisionRepository;
    private final CountryRepository countryRepository;

    public BootStrapData(CustomerRepository customerRepository, DivisionRepository divisionRepository, CountryRepository countryRepository)
    {
      this.customerRepository = customerRepository;
      this.divisionRepository = divisionRepository;
      this.countryRepository = countryRepository;
    }

    @Override
    public void run(String... args) throws Exception
    {

      if(customerRepository.count() > 1)
      {
        return;
      }

/*
      Customer Jim = new Customer();
      Jim.setFirstName("Jim");
      Jim.setLastName("Paul");
      Jim.setAddress("124 Main St");
      Jim.setPostal_code("12345");
      Jim.setPhone("123-456-7891");
*/
      Customer Jane = new Customer();
      Jane.setFirstName("Jane");
      Jane.setLastName("Smith");
      Jane.setAddress("456 Elm St");
      Jane.setPostal_code("54321");
      Jane.setPhone("987-654-3210");

      Customer Alice = new Customer();
      Alice.setFirstName("Alice");
      Alice.setLastName("Derby");
      Alice.setAddress("789 Oak St");
      Alice.setPostal_code("67890");
      Alice.setPhone("555-555-5555");

      Customer Bob = new Customer();
      Bob.setFirstName("Bob");
      Bob.setLastName("Williams");
      Bob.setAddress("101 Pine St");
      Bob.setPostal_code("98765");
      Bob.setPhone("777-777-7777");


      Customer Eve = new Customer();
      Eve.setFirstName("Eve");
      Eve.setLastName("Brown");
      Eve.setAddress("222 Cedar St");
      Eve.setPostal_code("13579");
      Eve.setPhone("888-888-8888");

      //customerRepository.save(Jim);
      customerRepository.save(Jane);
      customerRepository.save(Alice);
      customerRepository.save(Bob);
      customerRepository.save(Eve);

      customerRepository.findAll();
    }
  }

