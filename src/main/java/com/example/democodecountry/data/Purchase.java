package com.example.democodecountry.data;

import com.example.democodecountry.entities.Cart;
import com.example.democodecountry.entities.CartItem;
import com.example.democodecountry.entities.Customer;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Purchase {
    private Customer customer;
    private Cart cart;
    private Set<CartItem> cartItems;

}
