package com.example.democodecountry.dao;

import com.example.democodecountry.entities.Vacation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin("http://localhost:4200")
@Repository
public interface VacationRepository extends JpaRepository<com.example.democodecountry.entities.Vacation, Long> {
}
