package com.example.democodecountry.services;

import com.example.democodecountry.dao.CartRepository;
import com.example.democodecountry.dao.CustomerRepository;
import com.example.democodecountry.dao.ExcursionRepository;
import com.example.democodecountry.data.Purchase;
import com.example.democodecountry.data.PurchaseResponse;
import com.example.democodecountry.entities.Cart;
import com.example.democodecountry.entities.CartItem;
import com.example.democodecountry.entities.Customer;
import com.example.democodecountry.entities.StatusType;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
@CrossOrigin

@Service
public class CheckoutServiceImpl implements com.example.democodecountry.services.CheckoutService
{
  private final CartRepository cartRepository;
  private final com.example.democodecountry.dao.CartItemRepository cartItemRepository;
  private final com.example.democodecountry.dao.VacationRepository vacationRepository;
  private final com.example.democodecountry.dao.CustomerRepository customerRepository;
  private final ExcursionRepository excursionRepository;

  public CheckoutServiceImpl(com.example.democodecountry.dao.CustomerRepository customerRepository, CartRepository cartRepository,
                             com.example.democodecountry.dao.VacationRepository vacationRepository, ExcursionRepository excursionRepository,
                             com.example.democodecountry.dao.CartItemRepository cartItemRepository) {
    this.customerRepository = customerRepository;
    this.cartRepository = cartRepository;
    this.cartItemRepository = cartItemRepository;
    this.vacationRepository = vacationRepository;
    this.excursionRepository = excursionRepository;

  }

  @Override
  @Transactional
  public PurchaseResponse placeOrder(Purchase purchase)
  {
    // Generate an order tracking number
    String orderTrackingNumber = generateOrderTrackingNumber();
    purchase.getCart().setOrderTrackingNumber(orderTrackingNumber);

    purchase.getCart().setStatus(com.example.democodecountry.entities.StatusType.ordered);

    // Retrieve customer and cart information
    com.example.democodecountry.entities.Vacation vacation = purchase.getCartItems()
      .stream()
      .findFirst()
      .map(com.example.democodecountry.entities.CartItem::getVacation)
      .orElseThrow(() -> new IllegalArgumentException("Vacation cannot be null."));

    vacationRepository.save(vacation);

    com.example.democodecountry.entities.Cart savedCart = cartRepository.save(purchase.getCart());

    Optional.ofNullable(((com.example.democodecountry.entities.Vacation) vacation).getExcursions())
      .ifPresent(excursions -> excursions.forEach(excursion -> {
        if (excursion.getVacation() == null) {
          excursion.setVacation(vacation);

        }

        // Save the excursion
        excursionRepository.save(excursion);
      }));

    // Cart items
    purchase.getCartItems().forEach(cartItem -> {
      cartItem.setCart(savedCart);

      cartItemRepository.save(cartItem);
    });

    purchase.getCartItems().forEach(cartItem -> {
      Set<com.example.democodecountry.entities.Excursion> excursionsForCartItem = cartItem.getExcursions();
      if (excursionsForCartItem != null) {
        excursionsForCartItem.forEach(excursion -> {
          com.example.democodecountry.entities.Excursion persistedExcursion = excursionRepository.findById(excursion.getId()).orElse(null);
          if (persistedExcursion != null) {
            persistedExcursion.getCartItems().add(cartItem);
            excursionRepository.save(persistedExcursion);
          }
        });
      }
    });

    com.example.democodecountry.entities.Customer customer = purchase.getCustomer();
    customerRepository.save(customer);

    return new PurchaseResponse(orderTrackingNumber);
  }
  // Generate a unique identifier
  private String generateOrderTrackingNumber() {
    return UUID.randomUUID().toString();
  }
}
