package com.example.democodecountry.dao;

import com.example.democodecountry.entities.Division;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin("http://localhost:4200")
@Repository
public interface DivisionRepository extends JpaRepository<com.example.democodecountry.entities.Division, Long> {
}
