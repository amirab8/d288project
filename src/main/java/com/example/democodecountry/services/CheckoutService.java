package com.example.democodecountry.services;

import com.example.democodecountry.data.Purchase;
import com.example.democodecountry.data.PurchaseResponse;

public interface CheckoutService {
    PurchaseResponse placeOrder(Purchase purchase);
}
