package com.example.democodecountry.config;


import com.example.democodecountry.dao.CustomerRepository;
import com.example.democodecountry.entities.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class DataInitializer implements CommandLineRunner {

    private final CustomerRepository customerRepository;

    @Autowired
    public DataInitializer(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        if (customerRepository.count() == 0) {
            addSampleCustomers();
        }
    }

    private void addSampleCustomers() {

      // Customer customer1 = new Customer("John", "Doe", "123 Easy St", "55555", "(555)555-5555");

        Customer customer1 = new Customer("Jim", "Paul", "124 Main St", "12345", "123-456-7891");
        Customer customer2 = new Customer("Jane", "Smith", "456 Elm St", "54321", "987-654-3210");
        Customer customer3 = new Customer("Alice", "Derby", "789 Oak St", "67890", "(555)777-5555");
        Customer customer4 = new Customer("Bob", "Williams", "101 Pine St", "98765", "777-777-7777");
        Customer customer5 = new Customer("Eve", "Brown", "222 Cedar St", "13579", "888-888-8888");

        customerRepository.saveAll(List.of(customer1, customer2, customer3, customer4, customer5));

    }

}
