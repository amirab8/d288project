package com.example.democodecountry.controller;

import com.example.democodecountry.data.Purchase;
import com.example.democodecountry.data.PurchaseResponse;
import com.example.democodecountry.services.CheckoutService;
import org.springframework.web.bind.annotation.*;

@CrossOrigin("http://localhost:4200")
@RestController
@RequestMapping("/api/checkout")
public class CheckoutController
{

  private CheckoutService checkout;
  public CheckoutController(CheckoutService checkout) {
    this.checkout = checkout;
  }

  @PostMapping("/purchase")
  public PurchaseResponse placeOrder(@RequestBody Purchase purchase)
  {
    com.example.democodecountry.data.PurchaseResponse purchaseResponse = checkout.placeOrder(purchase);
    return purchaseResponse;
  }
}
