package com.example.democodecountry.services;

import com.example.democodecountry.entities.Cart;
import com.example.democodecountry.entities.CartItem;
import com.example.democodecountry.entities.Customer;
import com.example.democodecountry.entities.Vacation;
import lombok.*;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.Set;
@CrossOrigin
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Purchase
{
    private Customer customer;
    private Cart cart;
    private Set<CartItem> cartItems;

}
