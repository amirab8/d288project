package com.example.democodecountry.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
@CrossOrigin

//@CrossOrigin("http://localhost:4200")
@Entity
@Table(name = "cart_items")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class CartItem {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "cart_item_id")
  private Long id;

  @ManyToOne
  @JoinColumn(name = "vacation_id")
  private Vacation vacation;

  @ManyToMany(mappedBy = "cartItems")
  private Set<Excursion> excursions = new HashSet<>();

  @ManyToOne
  @JoinColumn(name = "cart_id")
  private Cart cart;

  @Column(name = "create_date")
  @CreationTimestamp
  private Date create_date;

  @Column(name = "last_update")
  @UpdateTimestamp
  private Date last_update;
}
